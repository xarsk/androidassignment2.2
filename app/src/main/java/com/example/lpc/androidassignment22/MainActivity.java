package com.example.lpc.androidassignment22;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        Button search = findViewById(R.id.search);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editTextID = findViewById(R.id.editTextUserID);
                String userID = editTextID.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("userid",userID);
                intent.setClassName("com.example.lpc.androidassignment22","com.example.lpc.androidassignment22.MapsActivity");

                startActivityForResult(intent,7);
            }

        });

        //SHOW ALL ENTRIES
        Button showAll = findViewById(R.id.ShowAllData);
        showAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName("com.example.user.myapplication","com.example.user.myapplication.ShowData");

                startActivityForResult(intent,7);
            }
        });

    }
}