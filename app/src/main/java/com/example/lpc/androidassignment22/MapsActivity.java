package com.example.lpc.androidassignment22;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        List<Marker> List = new ArrayList<>();

        Intent intent = getIntent();
        String userID = intent.getStringExtra("userid");

        String selection = "USERID=?";
        String [] selectionArgs = new String[]{userID};
        System.out.println(">>>"+ selection +" and " + selectionArgs[0]);
        //SELECT ROWS ACCORDING TO userID
        Cursor cursor = getContentResolver().query(
                Uri.parse("content://com.example.lpc.assignment1/location/userID"),
                null,
                selection,
                selectionArgs,
                null
        );

        if(cursor.getCount() == 0 ) {
            Toast.makeText(MapsActivity.this, "The DataBase is Empty", Toast.LENGTH_SHORT).show();
        }else {
            while(cursor.moveToNext()) {

                double currLon = Double.parseDouble(cursor.getString(2));
                double currLat = Double.parseDouble(cursor.getString(3));
                System.out.println("Latitude: "+currLat+" Longitude: "+currLon);
                LatLng currLoc = new LatLng(currLat,currLon);
                Marker currMark = mMap.addMarker(new MarkerOptions().position(currLoc)
                        .title("")
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                        .draggable(true));
//                currMark.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

                List.add(currMark);

                String currentData = "id:" +cursor.getString(0)+",User_id: " +cursor.getString(1)+",longitude: "+cursor.getString(2)+",latitude: "+cursor.getString(3)+",timestamp: "+cursor.getString(4);
                System.out.println(currentData);
            }
        }

        //move the camera
        for (Marker m: List) {
            //System.out.println(""+m.getPosition());
            LatLng latLng = new LatLng(m.getPosition().latitude,m.getPosition().longitude);
            mMap.addMarker(new MarkerOptions().position(latLng));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,16));
        }

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                LatLng newLatLon = marker.getPosition();
                System.out.println("LAT: "+newLatLon.latitude+"LON: "+newLatLon.longitude);
            }
        });


    }


}
