package com.example.lpc.androidassignment22;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ShowData extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_data);

        Intent intent = getIntent();
        String userID = intent.getStringExtra("user_id");

        ListView listView = (ListView) findViewById(R.id.listView2);
        ArrayList<String> dataList = new ArrayList<>();
        String selection = "USER_ID=?";
        String [] selectionArgs = new String[]{userID};

        //Select All
        Cursor cursor = getContentResolver().query(
                Uri.parse("content://com.example.user.project_1/location"),
                null,
                null,
                null,
                null
        );

        if(cursor.getCount() == 0 ) {
            Toast.makeText(ShowData.this, "The DataBase is Empty", Toast.LENGTH_SHORT).show();
        }else {
            while(cursor.moveToNext()) {
                String currentData = "id:" +cursor.getString(0)+",User_id: " +cursor.getString(1)+",longitude: "+cursor.getString(2)+",latitude: "+cursor.getString(3)+",timestamp: "+cursor.getString(4);
                dataList.add(currentData);

                ListAdapter listAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,dataList);
                listView.setAdapter(listAdapter);
            }
        }


    }
}
